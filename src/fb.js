// Initialize Firebase

import * as firebase from "firebase";

var config = {
    apiKey: "AIzaSyDQGl2eR2T9DWm1SZmUwuom-p33UhSN0Dc",
    authDomain: "heartland-home-improvements.firebaseapp.com",
    databaseURL: "https://heartland-home-improvements.firebaseio.com",
    projectId: "heartland-home-improvements",
    storageBucket: "heartland-home-improvements.appspot.com",
    messagingSenderId: "987009382361"
};
firebase.initializeApp(config);
export const authenticator = firebase.auth();
export const db = firebase.firestore();
export const storage = firebase.storage();


