import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import {MStore} from './store/MainStore'
import { authenticator } from './fb';
import Icon from 'vue-awesome/icons'
Vue.config.productionTip = false
Vue.component('icon', Icon)

var filter = function(text, length, clamp){
  clamp = clamp || '...';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
};
Vue.filter('truncate', filter);

new Vue({
  router,
  MStore,
  render: h => h(App),
  created() {
    authenticator.onAuthStateChanged((user) => {
      if (user) {
        console.log('Auth state changed');
        console.log(user);
        console.log('Calling auto sign in, fetch user data, and load favorite memes');

        // any call that has logic related to the currently signed in user must be done within this block
        MStore.dispatch('autoSignIn', user);
        MStore.dispatch('fetchUserData');

      }

    })
    MStore.dispatch('loadColors');
    MStore.dispatch('loadArticles');
    MStore.dispatch('loadFeatures');
    MStore.dispatch('loadAlbums');
    MStore.dispatch('loadBlogs');
    MStore.dispatch('loadConfig');
    MStore.dispatch('loadContactConfig')


  }
}).$mount('#app')

