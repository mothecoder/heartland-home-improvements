import {authenticator, db, storage} from "../fb";


export default {
    state: {
        submitted: false,
        requests: [ ]
    },
    mutations: {

        setDisplayName(state, payload){
            state.displayName = payload
        },
        setRequests(state, payload){
            state.requests = payload
        },
        setSubmitted(state, payload){
            state.submitted = payload
        },
        removeRequest(state, payload){
            state.requests = state.requests.filter(request => request.id !== payload.id)
        }

    },
    actions: {
        sendRequest({commit,getters}, payload){
            commit('setLoading', true)
            db.collection('requests').add(payload).then(()=>{
                commit('setSnackBar', `Submitted your request to ${getters.config.websiteOwnerName} successfully!`)
                commit('setLoading', false)
                commit('setSubmitted', true)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        deleteRequest({commit},payload){
           commit('setLoading', true)
           db.collection('requests').doc(payload.id).delete().then(()=>{
               commit('setLoading', false)
               commit('removeRequest', payload)
           }).catch(error=>{
               commit('setLoading', false)
               commit('setError', error)
           })
        },
        loadRequests({commit}){
            commit('setLoading', true)
            db.collection('requests').get().then(result=>{
                let temp = []
                result.forEach(request=> temp.push({...request.data(), id: request.id}))
                commit('setRequests', temp)
                commit('setLoading', false)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        }
    },
    getters: {
        submitted(state){
            return state.submitted
        },
        requests(state){
            return state.requests
        }

    }

}