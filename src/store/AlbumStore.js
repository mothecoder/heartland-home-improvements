import {db} from "../fb";
import {storage} from "../fb";
import router from "../router";

export default {
    state: {
        albums: [],
        images:[]
    },

    mutations:{
        setAlbums(state,payload){
            console.log('mutation::setAlbums')
            state.albums = payload
        },
        addAlbum(state, payload){
            console.log('Mutation::addAlbum')
            state.albums.push(payload)
        },

        deleteAlbum(state, payload){
            state.albums = state.albums.filter(Album=> Album.id !== payload)

        },
        updateAlbum(state, payload){
            console.log('Mutation::updateAlbum')
            let album = state.albums.find(Album=>{
                return Album.id === payload.id
            })
                album.src = payload.src;
                album.title= payload.title;
                album.description = payload.description;


        },
        addImageToAlbum(state, payload){
            console.log('Mutation::addImageToAlbum')
            state.images.push(payload.image)
        },
        deleteImage(state, payload){
            console.log("Mutation::deleteImage")
            state.images = state.images.filter(image=> {return image.id !== payload.id})
        },
        loadImages(state, payload){
            console.log('Mutation::loadImages')
            state.images = payload
        }

    },
    actions: {
        loadAlbums ({commit}) {
            console.log('loadAlbums')
            commit('setLoading', true)

            db.collection('albums').get().then(result=>{
                let tempAlbums = []
                result.forEach(Album => {
                    tempAlbums.push({...Album.data(), id: Album.id})
                })
                commit('setAlbums', tempAlbums)
                commit('setLoading', false)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        addAlbum({commit}, payload){
            commit('setLoading', true)
            console.log('addAlbum')
            let album = {
                title: payload.title,
                description: payload.description,
            }
            let id = null
            // First we add the album to the database.
            db.collection('albums').add(album).then(result=> {
                id = result.id
                // Then we insert the image that the user uploaded for the album into the storage
               return  storage.ref('albums/' + id ).put(payload.image)
            }).then( result => {
                // When that operation is completed, we receive a response object, and we access it's download URL
                return storage.ref().child(result.metadata.fullPath).getDownloadURL()
            }).then(fullUrl=> {
                album.src = fullUrl;
                // Then we update the database again with the URL
                return db.collection('albums').doc(id).update(album)
            }).then(()=>{
                // Finally we update our local cache with the changes
                album.id = id
                commit('addAlbum', album)
                commit('setLoading', false)
                commit('setSnackBar', 'Added an album successfully!')
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        addImage({commit}, payload){
            commit('setLoading', true)
            console.log('action::addImage')
            console.log(payload)
            const albumId = payload.album.id;
            let image = {
                title: payload.title,
                description: payload.description,
            }
            db.collection('albums').doc(payload.album.id).collection('images').add(image).then(result=> {
                image.id = result.id
                return storage.ref('albums/' + albumId+ '/'+ image.id  ).put(payload.image)
            }).then(url=> {
                return storage.ref().child(url.metadata.fullPath).getDownloadURL()
            }).then(fullUrl=> {
                image.src = fullUrl;
                return db.collection('albums').doc(payload.album.id).collection('images').doc(image.id).update(image)

            }).then(()=>{
                commit('addImageToAlbum', {image, albumId: payload.album.id})
                commit('setLoading', false)
                commit('setSnackBar', 'Added an image successfully!')
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },

        loadImages({commit}, payload){
            console.log('action::loadImages')
            db.collection('albums').doc(payload).collection('images').get().then(result => {
                let images = []
                result.forEach(image => {
                    var http = new XMLHttpRequest();

                    http.open('HEAD', image.data().src, false);
                    http.send();

                    if(http.status !== 403){
                        images.push({...image.data(), id: image.id})
                    }
                })
                commit('loadImages', images) // remember that we need to pass the album id sothat we can add the images to the proper album
                commit('setLoading', false)
            })
        },
        deleteAlbum({commit,dispatch,getters}, payload) {
            db.collection('albums').doc(payload.id).delete().then(result => {
                commit('setSnackBar', 'Deleted Album successfully...')
                if(getters.featuredEvents.filter(e=>e.id === payload.id).length > 0){
                    dispatch('removeFeature', payload)
                }
                storage.ref(`/albums/${payload.id}`).delete();
                commit('deleteAlbum', payload.id)
                router.push('/albums')
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        deleteImage({commit}, payload) {
            console.log('deleteImage')
            console.log(payload)
            db.collection('albums').doc(payload.album.id).collection('images').doc(payload.id).delete().then(result => {
                commit('setSnackBar', 'Deleted image successfully...')
                storage.ref(`/albums/${payload.album.title}/${payload.title}`).delete();
                commit('deleteImage', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        updateAlbum({commit,getters,dispatch}, payload){

            commit('setLoading', true)
            console.log('Updating album')
            console.log(payload)
            let data= {
                title: payload.title,
                description: payload.description,
                src: payload.src,
                id: payload.id
            }
            // we check if the user uploaded a new image.
            if(payload.image != null){
                // if he did then we need to replace the pre-existing one in the storage.
                storage.ref('albums/' + payload.id  ).put(payload.image).then(url=>{
                    return storage.ref().child(url.metadata.fullPath).getDownloadURL()
                }).then(fullPath=>{
                    data.src = fullPath
                    // we then update it's reference in the albums
                   return  db.collection('albums').doc(payload.id).update(data)
                }).then(()=>{
                    // Then we update the local cache for albums.
                    commit('setSnackBar', 'Updated Album Successfully!')
                    commit('updateAlbum', data)
                    console.log('done updating album now attempting to update the feature')
                    console.log(payload.id)
                    console.log(getters.features)
                    // if the album happens to be a featured album, we need to update it's reference in featuredEvents
                    if(getters.featuredEvents.filter(element=> element.id === payload.id).length > 0){
                        dispatch('setFeature', data)
                    }
                    commit('setLoading', false)
                }).catch(error=>{
                    commit('setLoading', false)
                    commit('setError', error)
                })
            }else{
                db.collection('albums').doc(payload.id).update(data ).then(()=>{
                    commit('setSnackBar', 'Updated Album Successfully!')
                    commit('updateAlbum', data)
                    commit('setLoading', false)
                }).catch(error=>{
                    commit('setLoading', false)
                    commit('setError', error)
                })
            }

        }
    },
    getters:{
        albums(state){
            return state.albums
        },
        getAlbumById(state){
            return id => state.albums.find(album=>album.id === id)
        },
        images(state){
            return state.images;
        }
    }

}