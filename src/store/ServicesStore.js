export default{
    state:{
        routes:[
            {
                icon : 'home',
                title: 'Home',
                link : '/'
            },
            {
                icon: 'person',
                title: 'About',
                link: '/about'
            }

        ],
        textBlocks:[
            {
                icon: 'color_lens',
                title: 'Modern Design',
                text: 'We will help you gain a competitive advantage in the market by utilizing our interface design experts, that will b'
            },
            {
                icon: 'flash_on',
                title: 'Fast Development',
                text: 'Look no further than Yaqeen tech for lightening fast development. With our cloud server technologies and cutting edge practices. ' +
                    'We are able to build you a vast array of powerful features with minimal effort'
            },
            {
                icon: 'build',
                title: 'Customization',
                text: 'We customize the websites to meet your unique business needs! That includes building an inventory system, payment gateway, tracking application, machine learning algorithms and much more. '
            }
        ],
    },
    getters:{
        routes(state){
            return state.routes
        },
        textBlocks(state){
            return state.textBlocks;
        },

    }

}