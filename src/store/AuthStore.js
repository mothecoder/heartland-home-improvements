import {authenticator, db} from "../fb";
import router from "../router";

export default {
    state: {
        user: null,
    },

    mutations: {
        setUser(state, payload) {
            state.user = payload;
        }
    },
    actions: {
        signIn({commit}, payload) {
            commit('setLoading', true)
            commit('clearError', null)
            authenticator.signInWithEmailAndPassword(payload.email, payload.password).then(
                () => {
                    //router.push('/')
                    commit('setSnackBar', 'You have signed in successfully... redirecting!')
                    setTimeout(() => {
                        commit('setLoading', false)
                        router.push('/')
                    }, 2000)
                }
            )
                .catch(
                    error => {
                        commit('setLoading', false)
                        commit('setError', error)
                    }
                )
        },
        autoSignIn({commit, getters}, payload) {
            commit('setUser', payload)
            commit('setSnackBar', 'Hello ' + getters.config.websiteOwnerName)
        },
        logout({commit}) {
            authenticator.signOut().then(() => {
                commit('setUser', null)
                commit('setSnackBar', 'You have been logged out')
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })

        },
    },
        getters: {
            auth(state) {
                return state.user !== null && state.user !== undefined
            },
            user(state) {
                return state.user
            }
        }
    }

