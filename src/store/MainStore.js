import Vue from 'vue'
import Vuex from 'vuex'
import AuthStore from "./AuthStore";
import UserStore from "./UserStore";
import ContactStore from "./ContactStore";
import SharedStore from "./SharedStore";
import GeneralConfigStore from "./GeneralConfigStore";
import EventStore from "./EventStore";
import ArticleStore from "./ArticleStore";
import AlbumStore from "./AlbumStore";
import BlogStore from "./BlogStore";
import ColorStore from "./ColorStore";
import ServicesStore from "./ServicesStore";



Vue.use(Vuex)

export const MStore = new Vuex.Store({
    modules:{
        user: UserStore,
        auth: AuthStore,
        contact: ContactStore,
        shared: SharedStore,
        general: GeneralConfigStore,
        event: EventStore,
        article: ArticleStore,
        album: AlbumStore,
        blog: BlogStore,
        color: ColorStore,
        services: ServicesStore
    }
})
