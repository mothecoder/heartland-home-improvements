import {db} from "../fb";
import {storage} from "../fb";

export default {
    state: {
        blogs: []
    },

    mutations:{
        setBlogs(state,payload){
            state.blogs = payload
        },
        addBlog(state, payload){
            state.blogs.push(payload)
        },
        deleteBlog(state, payload){
            state.blogs = state.blogs.filter(Blog=> Blog.id !== payload)
        },
        updateBlog(state, payload){
            let blog = state.blogs.find(Blog=>{
                return Blog.id === payload.id
            })
            blog.title= payload.title;
            blog.description = payload.description;



        }

    },
    actions: {
        loadBlogs ({commit}) {
            console.log('loadBlogs')
            db.collection('blogs').get().then(result=>{
                let tempBlogs = []
                result.forEach(Blog => {
                    tempBlogs.push({...Blog.data(), id: Blog.id})
                })
                commit('setBlogs', tempBlogs)
            })
        },
        addBlog({commit}, payload){
            commit('setLoading', true)
            console.log('addBlog')
            console.log(payload)
            const filename = payload.image.name;
            storage.ref('blogs/' + filename ).put(payload.image)
                .then(result => result.metadata.fullPath
                ).then(halfUrl=> {
                return storage.ref().child(halfUrl).getDownloadURL()
                    .then(fullUrl => fullUrl)
            }).then(fullUrl=> {
                let blog = {
                    title: payload.title,
                    description: payload.description,
                    src: fullUrl
                }
                db.collection('blogs').add(blog)
                commit('addBlog', blog)
                commit('setLoading', false)
                commit('setSnackBar', 'Added an blog successfully!')
            })
        },
        deleteBlog({commit}, payload) {
            db.collection('blogs').doc(payload).delete().then(result => {
                commit('setSnackBar', 'Deleted Blog successfully...')
                commit('deleteBlog', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })

        },
        updateBlog({commit}, payload){
            db.collection('blogs').doc(payload.id).update({
                title: payload.title,
                description: payload.description
            } ).then(()=>{
                commit('setSnackBar', 'Updated Blog Successfully!')
                commit('updateBlog', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        }
    },
    getters:{
        blogs(state){
            return state.blogs
        },
    }

}