import {authenticator, db, storage} from "../fb";

export default {
    state:{
        featuredEvents:[

        ]
    },

    mutations:{
        setFeatures(state,payload){
            state.featuredEvents = payload
        },
        setFeature(state, payload){
           console.log('Mutation::setFeature')
            if(state.featuredEvents.filter(element=> element.id === payload.id).length > 0) {

               let event = state.featuredEvents.find(element=> {
                  return element.id === payload.id
               })
                event.title = payload.title;
                event.description = payload.description;
                event.src = payload.src;
           } else{
                state.featuredEvents.push(payload)
            }
        },
        removeFeature(state, payload){
            state.featuredEvents = state.featuredEvents.filter(feature=> feature.id !== payload.id)
        }

    },
    actions: {
        setFeature({commit}, payload){
            commit('setLoading', true)
            console.log('Setting feature')
                db.collection('features').doc(payload.id).set(payload).then(() => {
                    commit('setFeature', payload)
                    commit('setSnackBar', 'Added/Updated Album in featured albums successfully!')
                    commit('setLoading', false)
                }).catch(error => {
                    commit('setLoading', false)
                    commit('setError', error)
                })
        },
        loadFeatures({commit}){
            commit('setLoading', true)
            db.collection('features').get().then(result => {
                let features =[];
                result.forEach(feature=> {
                    features.push(feature.data())
                })
                commit('setFeatures', features)
                commit('setLoading', false)
            }).catch(error => {
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        removeFeature({commit}, payload){
            commit('setLoading', true)
            console.log()
            db.collection('features').doc(payload.id).delete().then(() => {
                commit('removeFeature', payload)
                commit('setSnackBar', 'Removed album from features albums successfully!')
                commit('setLoading', false)
            }).catch(error => {
                commit('setLoading', false)
                commit('setError', error)
            })
        }
    },
    getters:{
        featuredEvents(state){
            return state.featuredEvents
        },
        isFeatured(state){
            return keyword => state.featuredEvents.filter(event=>event.id === keyword).length > 0
        }
    }

}