import {authenticator, db, storage} from "../fb";
import router from '../router'
import * as firebase from "firebase";

export default {
    state: {

        displayName: 'Joe',
        isAdmin : false,
        users: [
        ]

    },
    mutations: {


        setDisplayName(state, payload){
            state.displayName = payload
        },
        setUsers(state, payload) {
            state.users = payload
        },
        isAdmin(state, payload){
            state.isAdmin = payload
        }
    },
    actions: {
        fetchUserData({commit,dispatch, getters}) {
            commit('setLoading', true)
            db.collection('users').doc(getters.user.uid).get().then(userData => {
                commit('setLoading', false)
            }).then(()=> commit('setLoading', false)).catch(error => {
                commit('setError', error)
            })

        },
    },
    getters: {
        displayName(state){
            return state.displayName
        },
        users(state){
            return state.users;
        },
        isAdmin(state){
            return state.isAdmin;
        }
    }

}