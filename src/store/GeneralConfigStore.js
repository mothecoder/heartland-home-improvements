import {db} from "../fb";

export default {
    state:{
        contactConfig: {
            email: true,
            phone: true,
            name: true,
            textArea: true,
            category:true
        },

        config:{
            websiteOwnerName: 'Joe',
            websiteOwnerPicture: '',
            title: 'Sample Website',
            email: 'Ffibich63@gmail.com',
            phone: '877-989-3268',
            motto: 'Heartland Improvements',
            companyInfo: 'Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.\n' +
                '                                Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\n' +
                '                                Nullam in aliquet odio. Aliquam eu est vitae tellus bibendum tincidunt. Suspendisse potenti.',
            location:{
                city:'Omaha',
                country: 'US',
                address: 'TBD'
            },
            theme: true,
            pages: {
                 blogs: false,
                 contactUs: false,
                 career: false
                },
            socialMedia:[
                {
                    name: 'facebook',
                    icon: 'fab fa-facebook fa-2x',
                    link: 'https://www.facebook.com/halhuneidi',
                    visible: true
                },
                {
                    name: 'instagram',
                    icon: 'fab fa-instagram fa-2x',
                    link: 'https://www.instagram.com/halhuneidi/?utm_source=ig_profile_share&igshid=1i3a2qym86drx',
                    visible: true
                },
                {
                    name: 'linkedIn',
                    icon: 'fab fa-linkedin fa-2x',
                    link: 'https://www.linkedin.com/in/hani-al-huneidi-2a26388b',
                    visible: true
                },
                {
                    name: 'Twitter',
                    icon: 'fab fa-twitter fa-2x',
                    link: '',
                    visible: false
                },
                {
                    name: 'GooglePlus',
                    icon: 'fab fa-google-plus fa-2x',
                    link: '',
                    visible: false
                }

            ],

        },

        routes:{

        },
    },

    mutations:{
        setConfig(state,payload){
            console.log(state.config)
            if(payload.theme){
                state.config.theme = payload.theme
            }
            if(payload.location){
                state.config.location = payload.location
            }
            if(payload.email){
                state.config.email = payload.email
            }
            if(payload.phone){
                state.config.phone = payload.phone
            }
            if(payload.motto){
                state.config.motto = payload.motto
            }
            if(payload.title){
                state.config.title = payload.title
            }
            if(payload.socialMedia){
                state.config.socialMedia = payload.socialMedia
            }
            if(payload.websiteOwnerName){
                state.config.websiteOwnerName = payload.websiteOwnerName
            }
            if(payload.websiteOwnerPicture){
                state.config.websiteOwnerPicture = payload.websiteOwnerPicture
            }
            if(payload.companyInfo){
                state.config.companyInfo = payload.companyInfo
            }
        },
        setContactConfig(state, payload){
            state.contactConfig = payload
        }


    },
    actions: {
        loadConfig({commit}){
            commit('setLoading', true)
            db.collection('settings').doc('config').get().then(res =>{
                let config = {
                    theme: res.data().theme,
                    websiteOwnerName: res.data().websiteOwnerName,
                    title: res.data().title,
                    websiteOwnerPicture: res.data().websiteOwnerPicture,
                    socialMedia: res.data().socialMedia,
                    phone: res.data().phone,
                    companyInfo: res.data().companyInfo,
                    email: res.data().email,
                    location: res.data().location,
                    motto: res.data().motto
                }
                commit('setConfig', config)
                commit('setLoading', false)
            }).catch(e=> {
                console.log(e)
                commit('setError', e)
                commit('setLoading', false)
            })
        },
        updateConfig({commit}, payload){
            commit('setLoading', true)
            db.collection('settings').doc('config').update(payload).then(()=>{
                commit('setConfig', payload)
                commit('setLoading', false)
                commit('setSnackBar', 'Updated Configuration successfully!')
            }).catch(e=> {
                console.log(e)
                commit('setError', e)
                commit('setLoading', false)
            })

        }
        ,loadContactConfig({commit}){
            commit('setLoading', true)
            db.collection('settings').doc('contactConfig').get().then(result=>{
                commit('setLoading',false)
                commit('setContactConfig', {...result.data(), id: result.id})
            }).catch(e=> {
                console.log(e)
                commit('setError', e)
                commit('setLoading', false)
            })
        },
        updateContactConfig({commit}, payload){
            commit('setLoading', true)
            db.collection('settings').doc('contactConfig').set(payload).then(()=>{
                commit('setContactConfig', payload)
                commit('setLoading', false)
                commit('setSnackBar', 'Updated Contact Us Configuration successfully!')
            }).catch(e=> {
                console.log(e)
                commit('setError', e)
                commit('setLoading', false)
            })

        }
    },
    getters:{
        config(state){
            return state.config
        },
        theme(state){
            return state.config.theme;
        },
        socialMediaByProp(state){
            return prop=>  state.config.socialMedia.find(item=>item.name === prop)
        },
        contactConfig(state){
            return state.contactConfig;
        }
    }

}