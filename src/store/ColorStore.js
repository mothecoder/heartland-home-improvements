import {db} from "../fb";

export default {
    state:{
        styles:[
            {
                id:'1',
                name:'footer',
                style: {
                    backgroundColor: '#9068be'
                }
            },
            {
                id:'2',
                name:'toolbar',
                style:{
                    backgroundColor: '#9068be'
                }
            },
            {
              id:'3',
              name:'background',
              style:{
                  backgroundColor:'#9068be'
              }
            },
            {
                id:'4',
                name:'cardTitle',
                style:{
                    backgroundColor:'#9068be'
                }
            },
            {
                id:'5',
                name:'cardBody',
                style:{
                    backgroundColor:'#9068be'
                }
            },
            {
                id:'6',
                name:'cardBottom',
                style:{
                    backgroundColor:'#9068be'
                }
            },


],
        colors: [
    {
        text:'Light Blue',
        value:'#6ed3cf'
    },
    {
        text:'Soft Purple',
        value:'#9068be'
    },
    {
        text:'Grey',
        value:'#e1e8f0'
    },
    {
        text:'Red Overlaid',
        value:'#cd5554'
    },
    {
        text:'Comfortably Tan',
        value:'#c9af98'
    },
    {
        text:'Peachy Kreme',
        value:'#ed8a63'
    },
    {
        text:'Brown Bonnet',
        value:'#845007'
    },
    {
        text:'Dark Slate',
        value:'#1d1e22'
    },
    {
        text:'Silver Fox',
        value:'#d4d4dc'
    },
    {
        text:'Lime Green',
        value:'#7dce94'
    },
    {
        text:'Soft Gold',
        value:'#d1bfa7'
    },
    {
        text:'Rose Gold',
        value:'#bd8c7d'
    },
    {
        text: 'Onyx',
        value:'#49494b'
    }
        ]
    },

    mutations:{
        updateColors(state,payload){
            console.log(payload)
            Object.keys(payload).forEach(prop=>{
                if(payload[prop] !== null && payload[prop] !== undefined){
                    let result = state.styles.find(item=> item.name ===  prop)
                    result.style.backgroundColor = payload[prop]
                }
            })
        },

    },
    actions: {
        updateMainColors({commit},payload){
           commit('setLoading', true)
            db.collection('settings').doc('MainColors').update(payload).then(()=>{
                commit('setLoading', false)
                commit('setSnackBar', 'Color configuration updated successfully!')
                commit('updateColors', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
            commit('updateColors', payload)
        },
        loadColors({commit}){
            commit('setLoading', true)
            commit('setAppLoading', true)
            db.collection('settings').doc('MainColors').get().then(result=>{
                commit('setLoading', false)
                commit('setAppLoading', false)
                commit('updateColors', result.data())
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        }
    },
    getters:{
        getStyleByProp(state){
            return keyword => state.styles.find(style=>style.name === keyword)
        },
        getColors(state){
            return state.colors
        }

    }

}