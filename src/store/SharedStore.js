export default {
    state:{
        loading:false,
        error: null,
        snackBar: null,
        result: false,
        appLoading :false
    },

    mutations:{
        setLoading(state,payload){
            state.loading = payload
        },
        setError(state, payload){
            state.error = payload
        },
        clearError(state){
            state.error = null
        },
        setSnackBar(state,payload){
            state.snackBar = payload
        },
        clearSnackBar(state){
            state.snackBar = null
        },
        setAppLoading(state, payload){
            state.appLoading = payload
        }
    },
    actions: {
        clearError ({commit}) {
            commit('clearError')
        },
        clearSnackBar({commit}){
            commit('clearSnackBar')
        }
    },
    getters:{
        loading(state){
            return state.loading
        },
        error(state){
            return state.error
        },
        snackBar(state){
            return state.snackBar
        },
        appLoading(state){
            return state.appLoading
        }
    }

}