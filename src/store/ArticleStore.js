import {db} from "../fb";
import * as Vue from "vue";


export default {
    state: {
        articles: [],
        mainArticle: {}
    },

    mutations:{
        setArticles(state,payload){
            state.articles = payload
        },
        setMainArticle(state, payload){
          state.mainArticle = payload
        },
        addArticle(state, payload){
            state.articles.push(payload)
        },
        deleteArticle(state, payload){
            state.articles = state.articles.filter(article=> article.id !== payload)
        },
        updateArticle(state, payload){
            let article = state.articles.find(article=>{
                return article.id === payload.id
            })
            article.title= payload.title;
            article.description = payload.description;

        }

    },
    actions: {
        loadArticles ({commit}) {
            console.log('loadArticles')
            commit('setLoading', true)
            db.collection('articles').get().then(result=>{
                let tempArticles = []
                result.forEach(article => {
                    tempArticles.push({...article.data(), id: article.id})
                })
                commit('setArticles', tempArticles)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
            db.collection('articles').doc('MainArticle').get().then(result=>{
                commit('setLoading', false)
                commit('setMainArticle', result.data())
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        addArticle({commit}, payload){
            db.collection('articles').add(payload).then(result=>{
                commit('addArticle',{...payload, id: result.id})
                commit('setSnackBar', 'Added an article successfully!')
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        },
        deleteArticle({commit}, payload) {
            db.collection('articles').doc(payload).delete().then(result => {
                commit('setSnackBar', 'Deleted article successfully...')
                commit('deleteArticle', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })

        },
        updateArticle({commit}, payload){
            commit('setLoading', true)
            if(payload.isMain){
                db.collection('articles').doc('MainArticle').set(payload).then(()=>{
                    commit('setMainArticle', payload)
                    commit('setLoading', false)
                }).catch(error=>{
                    commit('setLoading', false)
                    commit('setError', error)
                })
            }
            db.collection('articles').doc(payload.id).update({
                title: payload.title,
                description: payload.description
            } ).then(()=>{
                commit('setSnackBar', 'Updated Article Successfully!')
                commit('updateArticle', payload)
            }).catch(error=>{
                commit('setLoading', false)
                commit('setError', error)
            })
        }
    },
    getters:{
        articles(state){
            return state.articles.filter(article => {return article.title !== state.mainArticle.title})
        },
        mainArticle(state){
            return state.mainArticle
        },
        isMain(state){
            return keyword => state.mainArticle.id === keyword;
        }
    }

}