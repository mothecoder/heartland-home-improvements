import Vue from 'vue'
import Router from 'vue-router'
import Home from "./components/Home";
import ContactUs from "./components/ContactUs";
import Signin from "./components/Signin";
import Admin from "./components/panel/Admin";
import Albums from "./components/album/Albums";
import Timeline from "./components/timeline/Timeline";
import Blogs from "./components/blog/Blogs";
import AlbumDetails from "./components/album/AlbumDetails";

Vue.use(Router)

export default new Router({
  routes: [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
      name: 'Albums',
      path: '/albums',
      component: Albums
    },
    {
      name: 'Album',
      path: '/albums/:id',
      component: AlbumDetails,
      props:true  // this enables vue to grab the id and add it to props.
    },
    {
      name: 'Contactus',
      path: '/contactus',
      component: ContactUs
    },
    {
      name: 'Career',
      path: '/blog',
      component: Blogs
    },
    {
      name:'Sigin',
      path:'/signin',
      component: Signin
    },
    {
      name:'Admin',
      path:'/admin',
      component: Admin
    },
    {
      name:'Requests',
      path:'/admin/requests',
      component: Admin
    },
    {
      name:'Timeline',
      path:'/timeline',
      component: Timeline
    }

  ],
  mode:'history'
})
